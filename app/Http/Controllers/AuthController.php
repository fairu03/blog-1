<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function daftar()
    {
        return view('daftar');
    }

    public function kirim(Request $request)
    {
        $namaawal = $request['firstname'];
        $namaakhir = $request['lastname'];
        $male = $request['male'];
        $nasional = $request['nationality'];
        $bio = $request['bio'];


        return view('welcome', compact('namaawal', 'namaakhir', 'male', 'nasional', 'bio'));
    }
}
